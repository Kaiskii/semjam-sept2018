﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class InitLevelBuild : MonoBehaviour {
    private string FilePath;

    public Image sel;
    public Text test;
    
    public string FileName;

    public GameObject TileTemplate;
    public Transform TileHolder;

    Sprite[] TileSet;

    public GameObject TileSetHolder;

    public List<GameObject> GeneratedTiles = new List<GameObject>();

    public int LevelWidth;
    public int LevelHeight;

    List<List<int>> Level = new List<List<int>>();

    private void Start() {
        TileSet = TileSetHolder.GetComponent<TileSet>().Tiles;

        for (int i = 0; i < LevelHeight; i++) {
            Level.Add(new List<int>());
            for(int j = 0; j < LevelWidth; j++) {
                Level[i].Add(0);
            }
        }

        RenderTile();
    }

    private void Update() {
        FileName = test.text;
        FilePath = Application.dataPath + "/Resources/" + FileName + ".txt";

        ControlScheme();
        ChangeTiles();
    }

    void ControlScheme() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene("MainMenu");
        }

        if (Input.GetKeyDown(KeyCode.Keypad4)) {
            LevelWidth--;
            for (int i = 0; i < Level.Count; i++) {
                Level[i].RemoveAt(LevelWidth);
            }
            RenderTile();
        }

        if (Input.GetKeyDown(KeyCode.Keypad6)) {
            LevelWidth++;
            for (int i = 0; i < Level.Count; i++) {
                Level[i].Add(0);
            }
            RenderTile();
        }

        if (Input.GetKeyDown(KeyCode.Keypad2)) {
            LevelHeight--;
            Level.RemoveAt(LevelHeight);
            RenderTile();
        }

        if (Input.GetKeyDown(KeyCode.Keypad8)) {
            LevelHeight++;
            Level.Add(new List<int>());

            for (int j = 0; j < LevelWidth; j++) {
                Level[LevelHeight - 1].Add(0);
            }

            RenderTile();
        }

        if (Input.GetKeyDown(KeyCode.F)) {
            TileFromFile();
            RenderTile();
        }

        if (Input.GetKeyDown(KeyCode.R)) {
            string Output = "";
            Output += "Height = " + LevelHeight + "\n" + "Width = " + LevelWidth + "\n\n";


            Output += "Tiles = ";
            for (int i = 0; i < LevelHeight; i++) {
                for (int j = 0; j < LevelWidth; j++) {
                    Output += Level[i][j];

                    if (j != LevelWidth - 1) {
                        Output += ",";
                    }
                }
                Output += "|";
            }

            Debug.Log("Success! File at: " + FilePath);
            File.WriteAllText(FilePath, Output);
            AssetDatabase.Refresh();
        }
    }

    void ChangeTiles() {
        if (Input.GetMouseButton(0)) {
            if (ScreenMouseRay() != null) {
                Collider2D[] SelectedTile = ScreenMouseRay();
                Tiles t = SelectedTile[0].GetComponent<Tiles>();
                Level[t.y][t.x] = sel.GetComponent<TileCycle>().curTile;
            }
        }
        RenderTile();
    }

    void RenderTile() {

        foreach (Transform t in TileHolder) {
            Destroy(t.gameObject);
        }

        GeneratedTiles.Clear();

        Vector2 origin = Vector2.zero;
        for (int i = 0; i < Level.Count; i++) {
            for (int j = 0; j < Level[i].Count; j++) {
                GameObject TempT = Instantiate(TileTemplate, new Vector3(j, i), Quaternion.identity, TileHolder);
                TempT.GetComponent<Tiles>().x = j;
                TempT.GetComponent<Tiles>().y = i;
                TempT.GetComponent<Tiles>().RenderSprite(TileSet[Level[i][j]]);
                GeneratedTiles.Add(TempT);
            }
        }
    }

    Collider2D[] ScreenMouseRay() {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = 1f;

        Vector2 v = Camera.main.ScreenToWorldPoint(mousePosition);

        Collider2D[] col = Physics2D.OverlapPointAll(v);

        if (col.Length > 0) {
            return col;
        }

        return null;
    }

    void TileFromFile() {
        TextAsset textFile = Resources.Load(FileName) as TextAsset;
        string data = textFile.text;

        string heightData = data.Substring(data.IndexOf("Height = ") + 9);
        heightData = heightData.Substring(0, heightData.IndexOf("\n"));

        LevelHeight = int.Parse(heightData);

        string widthData = data.Substring(data.IndexOf("Width = ") + 8);
        widthData = widthData.Substring(0, widthData.IndexOf("\n"));

        LevelWidth = int.Parse(widthData);

        string tileData = data.Substring(data.IndexOf("Tiles = ") + 8);

        Level.Clear();

        for (int i = 0; i < LevelHeight; i++) {
            Level.Add(new List<int>());
            for (int j = 0; j < LevelWidth; j++) {
                Level[i].Add(0);
            }
        }

        List<string> rowTData = new List<string>();

        for (int i = 0; i < LevelHeight; i++) {
            rowTData.Add(tileData.Substring(0, tileData.IndexOf("|")));

            tileData = tileData.Substring(tileData.IndexOf("|") + 1);

            string[] splitCData = rowTData[i].Split(',');

            for (int j = 0; j < LevelWidth; j++) {
                Level[i][j] = int.Parse(splitCData[j]);
            }
        }

        RenderTile();
    }
}
