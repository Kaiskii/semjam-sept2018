﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tiles : MonoBehaviour {
    public int x = 0;
    public int y = 0;

    SpriteRenderer sprRend;

    private void Awake() {
        sprRend = GetComponent<SpriteRenderer>();
    }

    public void RenderSprite(Sprite s) {
        sprRend.sprite = s;
    }
}
