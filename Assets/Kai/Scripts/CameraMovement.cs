﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    [Range(0, 20)]
    public float MaxSpeed;
    float Speed;

    private void Start() {
        Speed = MaxSpeed;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.LeftShift)) {
            Speed = MaxSpeed * 2;
        } else {
            Speed = MaxSpeed;
        }

        transform.position = new Vector3(transform.position.x + Time.deltaTime * Speed * Input.GetAxisRaw("Horizontal"), transform.position.y + Time.deltaTime * Speed * Input.GetAxisRaw("Vertical"), -10);
    }
}
