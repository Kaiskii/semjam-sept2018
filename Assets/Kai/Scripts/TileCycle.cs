﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileCycle : MonoBehaviour {

    Sprite[] TileSet;

    public GameObject TileSetHolder;

    public Button LeftButton;
    public Button RightButton;

    public int curBackTile = 0;
    public int curTile = 0;
    public int curForwardTile = 0;

    public void Start() {
        TileSet = TileSetHolder.GetComponent<TileSet>().Tiles;
    }

    public void Update() {
        curBackTile = curTile - 1;
        curForwardTile = curTile + 1;

        if (curBackTile < 0) {
            curBackTile = TileSet.Length - 1;
        }

        if(curForwardTile > TileSet.Length - 1) {
            curForwardTile = 0;
        }

        if (Input.GetKeyDown(KeyCode.Q)) {
            BackCycle();
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            ForwardCycle();
        }

        GetComponent<Image>().sprite = TileSet[curTile];
        LeftButton.GetComponent<Image>().sprite = TileSet[curBackTile];
        RightButton.GetComponent<Image>().sprite = TileSet[curForwardTile];
    }

    public void ForwardCycle() {
        if(curTile < TileSet.Length - 1) {
            curTile++;
        } else {
            curTile = 0;
        }
    }

    public void BackCycle() {
        if (curTile > 0) {
            curTile--;
        } else {
            curTile = TileSet.Length - 1;
        }
    }
}
