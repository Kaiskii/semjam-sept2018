﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGen : MonoBehaviour {

    public string FileName;
    
    public Transform TileHolder;

    Sprite[] TileSet;

    public GameObject TileSetHolder;

    public List<GameObject> GeneratedTiles = new List<GameObject>();

    int LevelWidth;
    int LevelHeight;

    List<List<int>> Level = new List<List<int>>();

    public int[] AbnormalTiles = {8, 13, 17, 18, 22, 23, 24};
    public GameObject[] TileGO;


    // Use this for initialization
    void Start () {
        TileSet = TileSetHolder.GetComponent<TileSet>().Tiles;
        
        if(SceneManager.GetActiveScene().name != "MainMenu") {
            FileName = SceneTransition.levelName;
        }

        TileFromFile();
        SpawnTile();
    }

    void SpawnTile() {
        for(int i = 0; i < Level.Count; i++) {
            for(int j = 0; j < Level[i].Count; j++) {
                int abnormalTileCheck = Array.IndexOf(AbnormalTiles, Level[i][j]);
                if (Level[i][j] != 0) {
                    GameObject TempT;
                    if (abnormalTileCheck > -1) {
                        TempT = Instantiate(TileGO[abnormalTileCheck + 1], new Vector3(j, i), Quaternion.identity, TileHolder);
                    } else {
                        TempT = Instantiate(TileGO[0], new Vector3(j, i), Quaternion.identity, TileHolder);
                        TempT.GetComponent<Tiles>().RenderSprite(TileSet[Level[i][j]]);
                    }
                    TempT.GetComponent<Tiles>().x = j;
                    TempT.GetComponent<Tiles>().y = i;
                    GeneratedTiles.Add(TempT);
                }
            }
        }
    }

    void TileFromFile() {
        TextAsset textFile = Resources.Load(FileName) as TextAsset;
        string data = textFile.text;

        string heightData = data.Substring(data.IndexOf("Height = ") + 9);
        heightData = heightData.Substring(0, heightData.IndexOf("\n"));

        LevelHeight = int.Parse(heightData);

        string widthData = data.Substring(data.IndexOf("Width = ") + 8);
        widthData = widthData.Substring(0, widthData.IndexOf("\n"));

        LevelWidth = int.Parse(widthData);

        string tileData = data.Substring(data.IndexOf("Tiles = ") + 8);

        Level.Clear();

        for (int i = 0; i < LevelHeight; i++) {
            Level.Add(new List<int>());
            for (int j = 0; j < LevelWidth; j++) {
                Level[i].Add(0);
            }
        }

        List<string> rowTData = new List<string>();

        for (int i = 0; i < LevelHeight; i++) {
            rowTData.Add(tileData.Substring(0, tileData.IndexOf("|")));

            tileData = tileData.Substring(tileData.IndexOf("|") + 1);

            string[] splitCData = rowTData[i].Split(',');

            for (int j = 0; j < LevelWidth; j++) {
                Level[i][j] = int.Parse(splitCData[j]);
            }
        }
    }
}
