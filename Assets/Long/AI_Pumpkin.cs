﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Pumpkin : MonoBehaviour {
    public GameObject playerObject;
    public GameObject player;
    float distanceFromPlayer;

    public Sprite pumpkin_Normal;
    public Sprite pumpkin_Noticed;
    public Sprite pumpkin_Attack;
    SpriteRenderer SPrender;
    Rigidbody2D rb2D;

    public int noticeRange = 12;
    public int attackRange = 5;

    bool hasAttacked = false;
	
    void Start()
    {
        SPrender = this.gameObject.GetComponent<SpriteRenderer>();
        rb2D = this.gameObject.GetComponent<Rigidbody2D>();

        playerObject = GameObject.FindWithTag("Player");
        player = playerObject.transform.GetChild(0).gameObject;
    }

	void Update () {
        distanceFromPlayer = Vector3.Distance(player.transform.position, this.transform.position);
        Debug.DrawRay(this.transform.position, (player.transform.position - this.transform.position));
        if (distanceFromPlayer <= attackRange && player.transform.position.y >= this.transform.position.y)
        {
            if(!hasAttacked)
            {
                Attack();
            }
        }
        else if (distanceFromPlayer <= noticeRange)
        {
            SPrender.sprite = pumpkin_Noticed;
        }
        else
        {
            SPrender.sprite = pumpkin_Normal;
            hasAttacked = false;
        }

    }

    void Attack()
    {
        hasAttacked = true;
        SPrender.sprite = pumpkin_Attack;
        //this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, 2);

        Vector3 direction = player.transform.position - this.transform.position;
        rb2D.velocity = direction.x * new Vector2(1, 1);
    }
}
