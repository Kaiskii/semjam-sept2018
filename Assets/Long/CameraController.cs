﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public GameObject player;
    public float cameraBottomLimit;
    public float cameraMaxScroll;
	
	// Update is called once per frame
	void Update () {

        float posY = player.transform.position.y;

        if(posY<cameraBottomLimit)
        {
            posY = cameraBottomLimit;
        }


        this.transform.position = Vector3.MoveTowards(this.transform.position,new Vector3(player.transform.position.x,posY,-10),cameraMaxScroll);
	}
}
