﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTransition : MonoBehaviour {
    // Update is called once per frame
    public static string levelName;

    public Text level;

    public GameObject Button;
    public string sceneToLoad;

	void Update() {
        if (SceneManager.GetActiveScene().name == "MainMenu")
            levelName = level.text;

        if (Button.gameObject == null)
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}
