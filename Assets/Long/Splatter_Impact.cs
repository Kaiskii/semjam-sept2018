﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splatter_Impact : MonoBehaviour {
    public GameObject splatter;
    GameObject player;

    [Tooltip("Minimum size")]
    [SerializeField]
    float minScale;

    [Tooltip("Maximum size")]
    [SerializeField]
    float maxScale;

    Vector2 rand;
    Rigidbody2D rb2D;

    void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
        rand.x = Random.Range(-50, 50);
        rand.y = Random.Range(10, 100);

        rb2D.AddForce(rand * Random.Range(5,8));
    }

    void Update()
    {
        if(this.transform.position.y < -20)
        {
            Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        GameObject newSplatter = Object.Instantiate(splatter, this.transform.position, Quaternion.identity,this.transform.parent);

        float rTz, scale;
        rTz = Random.Range(0, 360);
        scale = Random.Range(minScale, maxScale);

        newSplatter.transform.Rotate(0, 0, rTz);
        newSplatter.transform.localScale = new Vector3(scale, scale, scale);

        Destroy(this.gameObject);
    }
}
