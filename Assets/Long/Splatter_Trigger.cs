﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splatter_Trigger : MonoBehaviour
{
    public GameObject dropletToSpawn;
    public Transform holder;
    public int dropletAmount;

    void OnDisable()
    {
            for (int i = 0; i < dropletAmount; i++)
            {
                Object.Instantiate(dropletToSpawn, this.transform.position, Quaternion.identity,holder);
            }
    }
}
