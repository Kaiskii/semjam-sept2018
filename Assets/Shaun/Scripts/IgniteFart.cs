﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgniteFart : MonoBehaviour {

    [Tooltip("The prefab for the explosion")]
    [SerializeField] GameObject explosion;
    [Tooltip("Distance needed to be able to ignite fart cloud")]
    [SerializeField] float distance;
    [SerializeField] Collider2D[] interaction;
    [SerializeField] GameObject explosionHolder;

    private void Awake() {

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        interaction = null;
        interaction = Physics2D.OverlapCircleAll(this.transform.position, distance);
        if (Input.GetButtonDown("Ignite")) {
            //! Play ignite sound
            //SoundManager.Instance.PlaySound("Explosion11", 3);

            for (int i=0;i<interaction.Length;i++)
            {
                if(interaction[i].gameObject.CompareTag("Fart Cloud"))
                {
                    Transform fPos = interaction[i].gameObject.transform;
                    Instantiate(explosion, new Vector2(fPos.position.x, fPos.position.y), Quaternion.identity,explosionHolder.transform);
                    //! Play explosion sound
                    SoundManager.Instance.PlaySound("Explosion8", 3);

                    Destroy(interaction[i].gameObject);
                    break;
                }
            }
        }        
	}
}
