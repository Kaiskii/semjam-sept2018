﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    [SerializeField]Collider2D[] interaction;
    [SerializeField] GameObject explosion;
    float explosionTimer = 0.0f;
    bool isAlreadyExplode = false;
    float deathTimer = 0;
    [Tooltip("How long the each explosion lasts")]
    [SerializeField]float lifeTime;

    private void Start()
    {
        Transform t = this.GetComponent<Transform>();
        float rTz;
        rTz = Random.Range(0, 360);
        t.Rotate(0, 0, rTz);
        //! Play explosion sound
        SoundManager.Instance.PlaySound("Explosion8", 3);
    }

    private void Update()
    {
        explosionTimer += Time.deltaTime;
        deathTimer += Time.deltaTime;

        if(!isAlreadyExplode)
        {
            if (explosionTimer >= 0.1f)
            {
                interaction = Physics2D.OverlapCircleAll(this.transform.position, 1.5f);
                for (int i = 0; i < interaction.Length; i++)
                {
                    if (interaction[i].gameObject.CompareTag("Fart Cloud") || interaction[i].gameObject.CompareTag("Destructable") || interaction[i].gameObject.CompareTag("Enemy"))
                    {
                        Transform fPos = interaction[i].gameObject.transform;
                        Instantiate(this.gameObject, new Vector2(fPos.position.x, fPos.position.y), Quaternion.identity);
                        Destroy(interaction[i].gameObject);
                    }
                    else
                    {
                        isAlreadyExplode = true;
                    }
                }
                explosionTimer = 0f;
            }
        }
        
        if(deathTimer >= lifeTime)
        {
            Destroy(this.gameObject);
        }
    }

}
