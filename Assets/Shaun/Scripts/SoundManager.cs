﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour {

    [Tooltip("All the sound ")]
    public List<AudioClip> sounds;
    public List<AudioClip> deathSounds;
    public static SoundManager instance;
    public AudioSource bgm;
    public AudioSource playerSound;
    public AudioSource otherSound;
    public AudioSource deathSound;

    public static SoundManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if(instance != null && instance != this.gameObject)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    private void Start()
    {
        PlaySound("BGM",1);
    }

    private void Update() {
        if(!bgm.isPlaying) {
            PlaySound("BGM", 1);
        }
    }

    public void PlaySound(string soundName,int sourceIndex)
    {

        //Sound s = Array.Find(sounds, sound => sound.name == soundName);
        // s.source.Play();

        if(sourceIndex == 1)
        {
            for (int i = 0; i < sounds.Count; i++)
            {
                if (sounds[i].name == soundName)
                {
                    bgm.clip = sounds[i];
                    bgm.Play();
                    return;
                }
            }
        }

        if (sourceIndex == 2)
        {
            for (int i = 0; i < sounds.Count; i++)
            {
                if (sounds[i].name == soundName)
                {
                    playerSound.clip = sounds[i];
                    playerSound.Play();
                    return;
                }
            }
        }

        if (sourceIndex == 3)
        {
            for (int i = 0; i < sounds.Count; i++)
            {
                if (sounds[i].name == soundName)
                {
                    otherSound.clip = sounds[i];
                    otherSound.Play();
                    return;
                }
            }
        }

        if (sourceIndex == 4) {
            for (int i = 0; i < deathSounds.Count; i++) {
                if (deathSounds[i].name == soundName) {
                    deathSound.clip = deathSounds[i];
                    deathSound.Play();
                    return;
                }
            }
        }

    }

}
