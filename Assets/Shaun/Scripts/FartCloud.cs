﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FartCloud : MonoBehaviour {

    [Tooltip("Minimum size of the fart cloud")]
    [SerializeField] float minScale;

    [Tooltip("Maximum size of the fart cloud")]
    [SerializeField] float maxScale;

    [Tooltip("Roation per frame")]
    [SerializeField] float rotation;
    float direction;
    [SerializeField]float lifeTime;
    float lifeTimer = 0;
    
    // Use this for initialization
	void Start () {
        Transform t = this.GetComponent<Transform>();
        float scale,rt;
        rt = Random.Range(0, 360);
        scale = Random.Range(minScale, maxScale);
        t.Rotate(0,0,rt);
        t.localScale = new Vector3(scale, scale, scale);
        direction = Random.Range(1, 10);
    }
	
	// Update is called once per frame
	void Update () {
        lifeTimer += Time.deltaTime;

        if(lifeTimer >= lifeTime) {
            Destroy(this.gameObject);
        }        
        this.transform.Rotate(0,0,rotation * direction);
	}
}
