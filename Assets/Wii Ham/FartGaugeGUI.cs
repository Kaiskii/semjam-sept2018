﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FartGaugeGUI : MonoBehaviour {

    public Image PressureSprite;
    public Image TimerSprite;

    public Image PressureSprite2;
    public Image TimerSprite2;

    public Text LifeCounter;

    //public float FartGaugePercentage;
    //public float MaxFartGaugePercentage;
    [SerializeField] GameObject player;

    float PlayerCurrentPressure; // -- Updated (Pressure)
    float PlayerCurrentGas; // -- Updated (Gas)

    int Lives;
	
	// Update is called once per frame
	void Update () {
        PlayerCurrentPressure = player.GetComponent<Movement>().FartGauge*1280/600;
        PlayerCurrentGas = player.GetComponent<Movement>().MaxFartGauge*1280/600;

        Lives = 29-player.GetComponent<Movement>().DeathCounter;

        PressureSprite.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(PlayerCurrentPressure, 19);
        TimerSprite.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(PlayerCurrentGas, 19);

        PressureSprite2.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(PlayerCurrentPressure, 19);
        TimerSprite2.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(PlayerCurrentGas, 19);

        LifeCounter.text = "x " + Lives.ToString();
    }
}
