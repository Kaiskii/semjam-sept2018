﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAI : MonoBehaviour {

    public int speed = 0;
    public float TurnAroundDelay = 0;
    float Turn = 0;
    bool IsFacingRight = true;
    int Xaxis = 1;
    int Counter = 0;
    [SerializeField] Animator anim;


    // Use this for initialization
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {

        Turn += Time.deltaTime;

        if (Counter == 1) {
            if (Turn >= TurnAroundDelay * 2) {
                if (Xaxis == 1) {
                    Xaxis = -1;
                    Turn = 0;
                } else {
                    Xaxis = 1;
                    Turn = 0;
                }
            }
        } else {
            if (Turn >= TurnAroundDelay) {
                if (Xaxis == 1) {
                    Xaxis = -1;
                    Turn = 0;
                    Counter += 1;
                } else {
                    Xaxis = 1;
                    Turn = 0;
                    Counter += 1;
                }
            }
        }
        
            
        

        if (Xaxis == 1) {
            IsFacingRight = true;
        } else {
            IsFacingRight = false;
        }

        transform.position = new Vector2(transform.position.x + Time.deltaTime * Xaxis * speed, transform.position.y);
        if (IsFacingRight == true) {
            anim.Play("SkeletonRightWalk");
        } else {
            anim.Play("SkeletonLeftWalk");
        }

    }
}
