﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRespawn : MonoBehaviour {

    public GameObject Player;
    [SerializeField] ParticleSystem deathPS;
    int randDeathSound;
    SoundManager sm;

    private void Start() {
        sm = SoundManager.Instance.GetComponent<SoundManager>();
    }

    // Update is called once per frame
    void Update () {
        if (Player.activeSelf == false && Player.transform.GetChild(0).GetComponent<Movement>().DeathCounter < 30) {
            deathPS.transform.position = Player.transform.GetChild(0).transform.position;
            deathPS.Play();

            sm.PlaySound("brap", 2);
            randDeathSound = Random.Range(0, sm.deathSounds.Count);
            sm.PlaySound(sm.deathSounds[randDeathSound].name,4);

            transform.position = GameObject.FindGameObjectWithTag("pStart").transform.position;
            Player.transform.GetChild(0).position = this.transform.position;
            Player.transform.GetChild(0).GetComponent<Movement>().SetDead(false);
            Player.transform.GetChild(0).GetComponent<Movement>().JumpCounter = 2;
            Player.SetActive(true);
        }
        else if (Player.activeSelf == false && Player.transform.GetChild(0).GetComponent<Movement>().DeathCounter >= 30)
        {
            SceneManager.LoadScene("MainMenu");
        }
	}
}
