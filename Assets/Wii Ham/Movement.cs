﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Movement : MonoBehaviour {
    [Tooltip("Maximum Number of Jump")]
    public int MaxJump = 0;
    [Tooltip("Maximum Fart Gauge Length (in second)")]
    public int MaxFartGauge = 0;
    [Tooltip("Value to decrease in Fart Gauge every 1 secoond elapsed")]
    public float FartGaugeDecrement = 0;
    [Tooltip("Amount of increment for the Fart Gauge when gliding")]
    public int GlideFartGaugeIncrement = 0;
    public int SquatFart = 0;
    [Tooltip("Speed of Player Movement")]
    public float Speed = 0;
    [Tooltip("Jumping Force")]
    public float Force = 0;
    [Tooltip("Counter to keep track elapsed time, Do not change")]
    public float TimeCounter = 0;
    [Tooltip("Set Capacity for maximum fart force available for player to use when not on ground")]
    public float MaxFartForce = 0;
    public float FartForcePerSecDecrease = 0;
    //public float GlideForce = 0;



    [SerializeField] float FartForce = 0;
    float WorldTimeCounter = 0;
    public int JumpCounter = 0;
    public float FartGauge = 0;
    int MaxFartGaugeRecord = 0;
    bool TouchGround = false;
    bool DeadCheck = false;
    float TimeCounter2 = 0;
    Rigidbody2D rb;
    GameObject floor;
    [SerializeField] GameObject DeadBody;
    [SerializeField] public int DeathCounter;


    [SerializeField] ParticleSystem p1;
    [SerializeField] ParticleSystem p2;
    [SerializeField] ParticleSystem p3;

    [SerializeField] GameObject fartCloud;

    public List<GameObject> fartCloudCol = new List<GameObject>();

    Transform pPos;
    public Transform FartHolder;

    float fartCloudTimer = 0;
    [Tooltip("Spawn a fart cloud at every N second")]
    [SerializeField] float fartCloudSpawnRate;

    [SerializeField] Animator anim;

    [SerializeField] bool IsFacingRight = true;
    float glideDelay = 0;

    void Start() {
        Debug.Log("X = " + transform.position.x);
        rb = GetComponent<Rigidbody2D>();
        JumpCounter = MaxJump;
        //GlideForce = 0;
        FartForce = MaxFartForce;
        pPos = this.transform;
        MaxFartGaugeRecord = MaxFartGauge;
    }

    private void Update() {
        if (Input.GetKey(KeyCode.Escape)) {
            SceneManager.LoadScene("MainMenu");
        }

        //! Animation
        if (!(FartGauge >= MaxFartGauge || MaxFartGauge <= 0) && DeadCheck == false) {
            transform.position = new Vector2(transform.position.x + Time.deltaTime * Input.GetAxisRaw("Horizontal") * Speed, transform.position.y);
            if (Input.GetButton("S")) {
                fartCloudCol.Add(Instantiate(fartCloud, new Vector3(pPos.position.x, pPos.position.y, pPos.position.z), Quaternion.identity, FartHolder));
                FartGauge += SquatFart;
                if (!IsFacingRight) {
                    anim.Play("Left Crouch");
                } else {
                    anim.Play("Right Crouch");
                }
            }

            if (Input.GetButton("D")) {
                IsFacingRight = true;
                anim.Play("walk right");
            } else if (Input.GetButton("A")) {
                IsFacingRight = false;
                anim.Play("walk left");
            } else {
                if (!(FartGauge >= MaxFartGauge || MaxFartGauge <= 0) && DeadCheck == false) {
                    if (!Input.GetButton("S")) {
                        if (IsFacingRight) {
                            anim.Play("Right Idle");
                        } else {
                            anim.Play("Left Idle");
                        }
                    }
                }

            }
        }

        if (Input.GetButtonUp("Space")) {
            //! When button is up
            //! Stop the jump particle system if its still playing
            if (p1.isPlaying) {
                p1.Stop();
            }
            if (p2.isPlaying) {
                p2.Stop();
            }
            rb.gravityScale = 2;
            glideDelay = 0;
        }

        //! Check if player falls
        if (transform.position.y < -10) {
            DeadCheck = true;
        }

        //! Death
        if (DeadCheck == true) {
            DeathCounter += 1;
            transform.parent.gameObject.SetActive(false);

            //! Instantiate fart cloud on death
            fartCloudCol.Add(Instantiate(fartCloud, new Vector3(pPos.position.x, pPos.position.y, pPos.position.z), Quaternion.identity, FartHolder));

            GameObject DeadbodyObject = Instantiate(DeadBody, this.transform.position, Quaternion.identity);
            if (IsFacingRight == true) {
                DeadbodyObject.transform.GetChild(1).gameObject.SetActive(true);
            } else {
                DeadbodyObject.transform.GetChild(0).gameObject.SetActive(true);
            }
        }

        //! Max fart gauge
        WorldTimeCounter += Time.deltaTime;
        if (WorldTimeCounter >= 0.1) {
            MaxFartGauge--;
            if (FartGauge > 0) {
                FartGauge -= FartGaugeDecrement;
            }
            WorldTimeCounter = 0;
        }

        //! Fart Gauge
        if (FartGauge >= MaxFartGauge || MaxFartGauge <= 0) {
            DeadCheck = true;
        }
    }

    void FixedUpdate() {
        //! Jump
        if (JumpCounter > 0) {
            if (Input.GetButtonDown("Space")) {
                rb.velocity = new Vector2(0, Force);

                //! Play the jump particle system
                p1.Play();
                p2.Play();

                //! Instantiate the fart cloud
                fartCloudCol.Add(Instantiate(fartCloud, new Vector3(pPos.position.x, pPos.position.y, pPos.position.z), Quaternion.identity, FartHolder));

                SoundManager.Instance.PlaySound("Jump2", 2);

                JumpCounter--;
            }
        }

        //! Glide
        if (TouchGround == false) {


            if (Input.GetButton("Space")) {
                glideDelay += Time.deltaTime;
                if (glideDelay >= 0.3f) {
                    rb.velocity = (new Vector2(0, 0.1f) * FartForce);
                    //rb.AddForce(new Vector2(0, 0.1f) * FartForce);
                    rb.gravityScale = 1f;

                    fartCloudTimer += Time.deltaTime;

                    if (fartCloudTimer >= fartCloudSpawnRate) {
                        //! Instantiate the fart cloud
                        fartCloudCol.Add(Instantiate(fartCloud, new Vector3(pPos.position.x, pPos.position.y, pPos.position.z), Quaternion.identity, FartHolder));
                        SoundManager.Instance.PlaySound("brap", 2);

                        fartCloudTimer = 0;
                    }

                    TimeCounter += Time.deltaTime;
                    TimeCounter2 += Time.deltaTime;

                    if (TimeCounter2 >= 1) {
                        FartForce -= FartForcePerSecDecrease;

                        TimeCounter2 = 0;
                    }

                    if (TimeCounter >= 0.1) {
                        FartGauge += GlideFartGaugeIncrement / 10;

                        //! Stop the jump particle system and play the glide particle system
                        if (p1.isPlaying) {
                            p1.Stop();
                        }
                        if (p2.isPlaying) {
                            p2.Stop();
                        }
                        p3.Play();

                        TimeCounter = 0;
                    }
                }
            }

        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {

        if (collision.transform.CompareTag("Ground")) {
            TouchGround = true;
            JumpCounter = MaxJump;
            //GlideForce = 0;
            FartForce = MaxFartForce;
            TimeCounter = 0;
        }

        if (collision.transform.CompareTag("Touch & Die") || collision.transform.CompareTag("Enemy") || collision.transform.CompareTag("Mines")) {
            DeadCheck = true;
            if (collision.transform.CompareTag("Mines")) {
                Destroy(collision.gameObject);
            }
        }

        //! Stop the glide particle system if its still playing
        if (p3.isPlaying) {
            p3.Stop();
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.transform.CompareTag("Ground") || collision.transform.CompareTag("Destructable")) {
            TouchGround = false;
        }
    }

    public void SetDead(bool status) {
        DeadCheck = status;
        FartGauge = 0;
        MaxFartGauge = MaxFartGaugeRecord;
    }
}